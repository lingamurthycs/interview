package com.au;

import org.junit.Assert;
import org.junit.Test;

public class FizzBuzzTest {

    FizzBuzz fizzBuzz = new FizzBuzz();

    @Test
    public void returnsNumberForNumberNotDivisibleByThreeAndFive() {
        Assert.assertEquals(fizzBuzz.printFizzBuzz(1), "1");
    }

    @Test
    public void returnFizzForNumberDivisibleByThree() {
        Assert.assertTrue(fizzBuzz.printFizzBuzz(3).equalsIgnoreCase("Fizz"));
    }

    @Test
    public void returnBuzzForNumberDivisibleByFive() {
        Assert.assertTrue(fizzBuzz.printFizzBuzz(5).equalsIgnoreCase("Buzz"));
    }

    @Test
    public void returnsFizzBuzzForNumberDivisibleByThreeAndFive() {
        Assert.assertTrue(fizzBuzz.printFizzBuzz(15).equalsIgnoreCase("FizzBuzz"));
    }
}
