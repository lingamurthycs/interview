package com.au.cache;

import static org.junit.Assert.*;

import org.junit.Test;

public class LRUCacheTest {

    LRUCache cache;

    @Test
    public void testCacheWithCapacity1() {
        int capacity = 1;
        cache = new LRUCache(capacity);
        assertEquals(-1, cache.get(1));
        cache.set(1, 1);
        assertEquals(1, cache.get(1));
        cache.set(1, 2);
        assertEquals(2, cache.get(1));
        cache.set(2, 3);
        assertEquals(3, cache.get(2));
        assertEquals(-1, cache.get(1));
    }

    @Test
    public void testCacheWithCapacity3() {
        int capacity = 3;
        cache = new LRUCache(capacity);
        assertEquals(-1, cache.get(1));
        cache.set(1, 1);
        cache.set(1, 2);
        assertEquals(2, cache.get(1));
        cache.set(2, 11);
        cache.set(3, 12);
        cache.set(4, 13);
        assertEquals(-1, cache.get(1));
        assertEquals(11, cache.get(2));
        assertEquals(12, cache.get(3));
        assertEquals(13, cache.get(4));
        cache.set(5, 14);
        assertEquals(-1, cache.get(2));
        assertEquals(14, cache.get(5));
    }

}
